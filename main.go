package main

import (
	"log"
	"strings"
	"time"

	"github.com/itsubaki/gostream-core/pkg/event"
	"github.com/itsubaki/gostream-core/pkg/function"
	"github.com/itsubaki/gostream-core/pkg/window"
)

type Event struct {
	Time    time.Time
	IP      string
	Message string
}

type StringSearcher struct {
	Name    string
	Pattern string
}

func (s StringSearcher) Select(e event.Event) bool {
	return strings.Contains(
		strings.ToLower(e.String(s.Name)),
		strings.ToLower(s.Pattern))
}

func main() {
	// Setup time window: 30 seconds
	w := window.NewTime(Event{}, 30*time.Second)
	defer w.Close()

	log.Printf("Starting ...")

	// We want just a raw number
	w.SetFunction(
		function.Count{
			As: "count",
		},
	)

	// filter by our own selector
	w.SetSelector(
		&StringSearcher{
			Name:    "Message",
			Pattern: "failed password for",
		},
	)

	// Display results
	go func() {
		ticker := time.NewTicker(5 * time.Second)
		defer ticker.Stop()

		for range ticker.C {
			newest := event.Newest(<-w.Output())
			if count := newest.RecordInt("count"); count > 3 {
				e := newest.Underlying.(Event)
				log.Printf("[ALARM] SSH brute-force detected from %s, count: %d", e.IP, count)
			}
		}
	}()

	msgs := []string{
		"IPv6: ADDRCONF(NETDEV_CHANGE): wlp0s20f0u4u1: link becomes ready",
		"Failed password for invalid user root",
		"Failed password for invalid user admin",
		"Failed password for invalid user user",
		"device wlp0s20f0u4u1 left promiscuous mode",
		"usb 1-4.4.4: ath9k_htc: Transferred FW: ath9k_htc/htc_9271-1.4.0.fw, size: 51008",
		"Failed password for invalid user support",
		"Failed password for invalid user qwerty",
		"logitech-hidpp-device 0003:046D:4069.0070: multiplier = 8",
	}

	// Feed some data
	for _, v := range msgs {
		w.Input() <- Event{
			Message: v,
			IP:      "8.8.8.8",
			Time:    time.Now(),
		}
		time.Sleep(3 * time.Second)
	}

	// Wait a bit
	time.Sleep(2 * time.Second)
}
